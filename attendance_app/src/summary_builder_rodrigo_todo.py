import datetime

# dict is a reserverd keyword in Python DO NOT name variables, parmams, etc as dict
# what if dict[key] does not exists? BETTER to use get()
# in this case I am getting keys first then is ok!
def find_values(key, dict):
    if key in dict.keys():
        expected_value = dict[key]
        return expected_value

    # CR Note: I think this is not required because you are using process_especial_case to handle this
    return dict['Meeting Title']

11/15/22, 4:56:11 PM
10/18/2022, 5:50:03 PM

# TODO: 1) REFACTOR have single diff_time function ??
# RECOMENDED research datetime/timestamp diff funtionality

def dif_time(start, end):
    list_duration = []
    start_time = datetime.strptime(start[0:-3].replace(",", ""), '%m/%d/%y %I:%M:%S')
    end_time = datetime.strptime(end[0:-3].replace(",", ""), '%m/%d/%y %I:%M:%S')
    duration = end_time - start_time
    list_duration.append(int(duration.seconds / 3600))
    list_duration.append((int(duration.seconds) / 60) % 60)
    list_duration.append(int(duration.seconds) % 60)
    if list_duration[0] < 1:
        list_duration[0] = 0
    if list_duration[1] < 1:
        list_duration[1] = 0
    return list_duration

def dif_time_v2(start, end):
    list_duration = []
    start_time = datetime.strptime(start[0:-3].replace(",", ""), '%m/%d/%Y %I:%M:%S')
    end_time = datetime.strptime(end[0:-3].replace(",", ""), '%m/%d/%Y %I:%M:%S')
    duration = end_time - start_time
    list_duration.append(int(duration.seconds / 3600))
    list_duration.append((int(duration.seconds) / 60) % 60)
    list_duration.append(int(duration.seconds) % 60)
    if list_duration[0] < 1:
        list_duration[0] = 0
    if list_duration[1] < 1:
        list_duration[1] = 0
    return list_duration


def process_especial_case(raw_data):
    start = find_values('Meeting Start Time', raw_data)
    end = find_values('Meeting End Time', raw_data)
    duration = dif_time_v2(start, end)
    summary_dict = {
        'Meeting Title': find_values('Meeting Title', raw_data),
        'Id': raw_data.get('Meeting Id'),
        'Attended participants': int(find_values('Total Number of Participants', raw_data)),
        'Start Time': raw_data.get('Meeting Start Time'),
        'End Time': raw_data.get('Meeting End Time'),

        # This should point to object of class "Duration"
        # 'Duration': {
        #     'hours': duration_obj.hours,
        #     'minutes': duration_obj.minutes,
        #     'seconds': duration_obj.seconds
        # }

        'Duration': {
            'hours': int(duration[0]), # duration_obj.hours
            'minutes': int(duration[1]), # duration_obj.minutes
            'seconds': int(duration[2])  # duration_obj.seconds
        }
    }
    return summary_dict


def normalize_summary(raw_data: dict):
    if 'Meeting Start Time' in raw_data:
        data = process_especial_case(raw_data)
        return data

    # TODO: you can easily read values based on key using dict's get method
    start = data.get('Start time') or None # find_values('Start time', raw_data)
    end = find_values('End time', raw_data)
    duration = dif_time(start, end)
    summary_dict = {
        'Meeting Title': find_values('Meeting Title', raw_data),
        'Id': find_values('id', raw_data),
        'Attended participants': int(find_values('Attended participants', raw_data)),
        'Start Time': start,
        'End Time': end,
        'Duration': {
            'hours': duration[0],
            'minutes': duration[1],
            'seconds': duration[2]
        }
    }
    return summary_dict
















def build_summary_object(raw_data: dict):
    pass